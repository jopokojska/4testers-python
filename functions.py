def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)


def upper_word(word: str) -> str:
    return word.upper()


def add_two_numbers(a, b):
    return a + b


def calculate_square_number(a):
    return a ** 2


def calculate_volume_of_cuboid(a, b, c):
    return a * b * c


def convert_celsius_to_faranheit(temperature_in_celsius):
    return temperature_in_celsius * 1.8 + 32


def print_welcome_in_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")

def find_3_biggest_numbers_form_list(list):
    sorted_list = sorted(list)
    return sorted_list[-3:]


if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)
    big_dog = upper_word("dog")
    print(big_dog)
    print(add_two_numbers(3, 5))
    print(add_two_numbers(True, False))

    number_1 = 16
    number_2 = 0
    number_3 = 25

    print(calculate_square_number(number_1))
    print(calculate_square_number(number_2))
    print(calculate_square_number(number_3))

    print(calculate_volume_of_cuboid(3, 5, 7))
    print(convert_celsius_to_faranheit(20))
    print_welcome_in_city("Michał", "Toruń")

    print(find_3_biggest_numbers_form_list([5, 7, 14, 1, -8, 22]))
