friend_name = "Kasia"
friend_age = 28
pet_number = 0
has_driving_licence = True
friendship_years = 5.5

print("Name:", friend_name, sep="\n")
print("Age:", friend_age, sep="\n")
print("Pet number:", pet_number, sep="\n")
print("Has driving licence:", has_driving_licence, sep="\n")
print("Friendship years:", friendship_years, sep="\n")

print("********")

print(
    "Name:", friend_name,
    "Age:", friend_age,
    "Pet number:", pet_number,
    "Has driving licence:",
    has_driving_licence,
    "Friendship years:", friendship_years,
    sep="\t"
)

name_surname = "Joanna Pokojska"
email = "email@email.com"
phone = "123654789"

bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone

print("Name:", name_surname, "Email:", email, "Phone:", phone)
print("****")

print(bio)
print("****")

bio_smarter = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone}"

print(bio_smarter)
print("****")

bio_docstring = f"""Name: {name_surname}
Email: {email}
Phone: {phone}"""
print(bio_docstring)