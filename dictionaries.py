def get_person_data_dictionary(email, phone, city, street):
    return {
        "contact": {
            "email": email,
            "phone": phone
        },
        "address": {
            "city": city,
            "street": street
        }
    }


if __name__ == '__main__':
    my_pet = {"name": "Maks",
              "species": "dog",
              "age": 12,
              "weight": 13,
              "is_male": True,
              "fav_meal": ["carrots", "meat", "chips"]}

    print(my_pet["weight"])
    my_pet["weight"] = 10
    my_pet["like_swimming"] = True
    del my_pet["is_male"]
    print(my_pet)

    payload = get_person_data_dictionary("email.com", "123655454", "Gdańsk", "Startowa")
    print(payload)